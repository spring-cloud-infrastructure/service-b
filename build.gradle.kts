import org.gradle.api.tasks.testing.logging.TestLogEvent.FAILED
import org.gradle.api.tasks.testing.logging.TestLogEvent.PASSED
import org.gradle.api.tasks.testing.logging.TestLogEvent.SKIPPED
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.springframework.boot.gradle.tasks.bundling.BootBuildImage
import org.springframework.boot.gradle.tasks.bundling.BootJar

plugins {
    val springBootPluginVersion = "2.4.4"
    val springDependencyManagementPluginVersion = "1.0.11.RELEASE"
    val buildInfoPluginVersion = "0.1.3"
    val kotlinPluginVersion = "1.4.31"

    id("org.springframework.boot") version springBootPluginVersion
    id("io.spring.dependency-management") version springDependencyManagementPluginVersion
    id("com.pasam.gradle.buildinfo") version buildInfoPluginVersion
    kotlin("jvm") version kotlinPluginVersion
    kotlin("plugin.spring") version kotlinPluginVersion
    kotlin("kapt") version kotlinPluginVersion
}

group = "com.spring.cloud"
version = "1.0.0-SNAPSHOT"
val javaVersion = JavaVersion.VERSION_15

repositories {
    mavenLocal()
    mavenCentral()
    maven {
        name = "GitLab"
        url = uri("https://gitlab.com/api/v4/groups/9994989/-/packages/maven")
    }
}

dependencies {
    val springCloudStarterVersionAwareServiceDiscoveryClientVersion = "1.0.0-SNAPSHOT"
    val springCloudStarterResourceServerVersion = "1.0.0-SNAPSHOT"
    val logstashLogbackEncoderVersion = "6.6"
    val kotlinLoggingJvmVersion = "2.0.6"
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))
    implementation("com.spring.cloud:spring-cloud-starter-version-aware-service-discovery-client:$springCloudStarterVersionAwareServiceDiscoveryClientVersion")
    implementation("org.springframework.cloud:spring-cloud-starter-bootstrap")
    implementation("org.springframework.cloud:spring-cloud-starter-config")
    implementation("org.springframework.cloud:spring-cloud-starter-bus-amqp")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.retry:spring-retry")
    implementation("org.springframework:spring-aspects")
    implementation("org.springframework.cloud:spring-cloud-starter-sleuth")
    implementation("org.springframework.cloud:spring-cloud-sleuth-zipkin")
    implementation("net.logstash.logback:logstash-logback-encoder:$logstashLogbackEncoderVersion")
    implementation("io.github.microutils:kotlin-logging-jvm:$kotlinLoggingJvmVersion")
    implementation("com.spring.cloud:spring-cloud-starter-resource-server:$springCloudStarterResourceServerVersion")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.cloud:spring-cloud-starter-openfeign")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")

    kapt("org.springframework.boot:spring-boot-configuration-processor")

    val springMockkVersion = "3.0.1"
    val assertkVersion = "0.23.1"
    val kotlinWiremockVersion = "1.0.0"
    testImplementation("com.ninja-squad:springmockk:$springMockkVersion")
    testImplementation("com.willowtreeapps.assertk:assertk:$assertkVersion")
    testImplementation("com.marcinziolo:kotlin-wiremock:$kotlinWiremockVersion")
    testImplementation("org.springframework.cloud:spring-cloud-starter-contract-stub-runner")
    testImplementation("org.springframework.security:spring-security-test")
    testImplementation("org.springframework.amqp:spring-rabbit-test")
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(module = "mockito-core")
        exclude(module = "mockito-junit-jupiter")
    }
}

dependencyManagement {
    val springCloudDependenciesBomVersion = "2020.0.2"
    imports {
        mavenBom("org.springframework.cloud:spring-cloud-dependencies:$springCloudDependenciesBomVersion")
    }
}

configure<JavaPluginConvention> {
    sourceCompatibility = javaVersion
    targetCompatibility = javaVersion
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = javaVersion.toString()
    }
}

tasks.getByName<BootJar>("bootJar") {
    mainClass.set("com.spring.cloud.ServiceBApplicationKt")
}

tasks.getByName<BootBuildImage>("bootBuildImage") {
    imageName = "${project.group}/${project.name}"
    environment = mapOf(
        "BP_DEBUG_ENABLED" to "true"
    )
}

tasks.withType<Test> {
    useJUnitPlatform()
    testLogging {
        showExceptions = true
        showStandardStreams = true
        events(PASSED, SKIPPED, FAILED)
    }
}
