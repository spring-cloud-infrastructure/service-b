# Service B

Dummy business service.

## 1. Features

- [Open Feign](https://github.com/OpenFeign/feign) client (call
  to [Service A](https://gitlab.com/spring-cloud-infrastructure/service-a))

## 2. Configuration

- [bootstrap.yml](./src/main/resources/bootstrap.yml) (application bootstrap only)
- [config-repo](https://gitlab.com/spring-cloud-infrastructure/config-repo/-/tree/master/config) (externalized
  configuration)

## 3. Build & Run

Check out [docker](https://gitlab.com/spring-cloud-infrastructure/docker) project.
