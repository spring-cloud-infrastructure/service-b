package com.spring.cloud.config

import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.web.servlet.invoke

@Configuration
internal class WebSecurityConfiguration : WebSecurityConfigurerAdapter() {

    companion object {
        private const val ACTUATOR_URL_PATTERN = "/actuator/**"
    }

    override fun configure(http: HttpSecurity) = http {
        authorizeRequests {
            authorize(ACTUATOR_URL_PATTERN, permitAll)
            authorize(anyRequest, authenticated)
        }

        csrf {
            disable()
        }
    }
}
