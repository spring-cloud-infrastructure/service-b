package com.spring.cloud.config.messaging

import org.springframework.amqp.rabbit.annotation.EnableRabbit
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter
import org.springframework.amqp.support.converter.MessageConverter
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@EnableRabbit
internal class MessagingConfiguration {

    @Bean
    fun messagingJsonConverter(): MessageConverter = Jackson2JsonMessageConverter()
}
