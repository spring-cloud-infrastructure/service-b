package com.spring.cloud.messaging.listener.model

import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotBlank

data class HelloServiceMessage(
    @JsonProperty("message") @NotBlank val message: String,
)
