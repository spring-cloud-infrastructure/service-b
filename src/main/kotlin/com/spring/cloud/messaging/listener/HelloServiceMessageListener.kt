package com.spring.cloud.messaging.listener

import com.spring.cloud.messaging.listener.model.HelloServiceMessage
import mu.KotlinLogging.logger
import org.springframework.amqp.rabbit.annotation.Queue
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.messaging.Message
import org.springframework.stereotype.Component

@Component
internal class HelloServiceMessageListener {

    private val logger = logger { }

    @RabbitListener(
        queuesToDeclare = [
            Queue("\${services.service-b.messaging.binding.hello-service.queue}")
        ]
    )
    fun receiveHelloServiceMessage(message: Message<HelloServiceMessage>) =
        logger.info { "Hello service message received: [${message.payload.message}]" }
}
