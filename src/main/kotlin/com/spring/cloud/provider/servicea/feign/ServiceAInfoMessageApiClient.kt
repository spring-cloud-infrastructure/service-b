package com.spring.cloud.provider.servicea.feign

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.http.HttpHeaders.AUTHORIZATION
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestHeader

@FeignClient("service-a")
interface ServiceAInfoMessageApiClient {

    @GetMapping("info-message")
    fun getInfoMessage(@RequestHeader(AUTHORIZATION) authorizationHeader: String): ResponseEntity<String>
}
