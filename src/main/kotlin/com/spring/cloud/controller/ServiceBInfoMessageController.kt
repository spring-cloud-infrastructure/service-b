package com.spring.cloud.controller

import com.spring.cloud.provider.servicea.feign.ServiceAInfoMessageApiClient
import com.spring.cloud.resourceserver.client.authority.service.b.HasReadInfoMessageBAuthority
import mu.KotlinLogging.logger
import org.apache.commons.lang3.StringUtils.SPACE
import org.springframework.beans.factory.annotation.Value
import org.springframework.cloud.context.config.annotation.RefreshScope
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.ok
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RefreshScope
internal class ServiceBInfoMessageController(
    @Value("\${services.service-b.controller.ServiceBInfoMessageController.info-message}") private val infoMessage: String,
    private val serviceAInfoMessageApiClient: ServiceAInfoMessageApiClient,
) {

    private val logger = logger { }

    @GetMapping("info-message")
    @HasReadInfoMessageBAuthority
    fun getInfoMessage(authentication: JwtAuthenticationToken): ResponseEntity<String> {
        logger.info { "User: [${authentication.name}] with granted authorities: ${authentication.authorities} is accessing info message" }
        val serviceAResponse = serviceAInfoMessageApiClient.getInfoMessage("Bearer ${authentication.token.tokenValue}")
        return ok("${serviceAResponse.body}$SPACE$infoMessage")
    }
}
