package com.spring.cloud

import com.github.tomakehurst.wiremock.WireMockServer
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.cloud.client.DefaultServiceInstance
import org.springframework.cloud.client.ServiceInstance
import org.springframework.cloud.loadbalancer.core.RoundRobinLoadBalancer
import org.springframework.cloud.loadbalancer.core.ServiceInstanceListSupplier
import org.springframework.cloud.loadbalancer.support.SimpleObjectProvider
import org.springframework.context.annotation.Bean
import reactor.core.publisher.Flux
import reactor.kotlin.core.publisher.toFlux
import reactor.kotlin.core.publisher.toMono

@TestConfiguration
internal class TestLoadBalancerConfiguration {

    companion object {
        private const val SERVICE_A_ID = "service-a"
        private const val WIREMOCK_SERVER_HOST = "localhost"
    }

    @Bean
    fun serviceALoadBalancer(wiremock: WireMockServer): RoundRobinLoadBalancer {
        val serviceInstanceListSupplier = ServiceAServiceInstanceListSupplier(wiremock)
        return RoundRobinLoadBalancer(SimpleObjectProvider(serviceInstanceListSupplier), SERVICE_A_ID)
    }

    internal inner class ServiceAServiceInstanceListSupplier(
        private val wiremock: WireMockServer,
    ) : ServiceInstanceListSupplier {

        override fun get(): Flux<MutableList<ServiceInstance>> {
            val serviceInstance: ServiceInstance = DefaultServiceInstance(
                SERVICE_A_ID,
                SERVICE_A_ID,
                WIREMOCK_SERVER_HOST,
                wiremock.port(),
                false
            )
            return mutableListOf(serviceInstance).toMono().toFlux()
        }

        override fun getServiceId(): String {
            return SERVICE_A_ID
        }
    }
}
