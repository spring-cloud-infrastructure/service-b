package com.spring.cloud.controller

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.equalTo
import com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo
import com.marcinziolo.kotlin.wiremock.equalTo
import com.marcinziolo.kotlin.wiremock.get
import com.marcinziolo.kotlin.wiremock.returns
import com.spring.cloud.ServiceBApplication
import com.spring.cloud.TestLoadBalancerConfiguration
import org.apache.commons.lang3.StringUtils.SPACE
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock
import org.springframework.http.HttpHeaders.AUTHORIZATION
import org.springframework.http.MediaType.TEXT_PLAIN
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.jwt
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get

@SpringBootTest(
    webEnvironment = RANDOM_PORT,
    classes = [
        ServiceBApplication::class
    ]
)
@AutoConfigureMockMvc
@AutoConfigureWireMock
@SpringJUnitConfig(TestLoadBalancerConfiguration::class)
internal class ServiceBInfoMessageControllerIntegrationTest(
    @Value("\${services.service-b.controller.ServiceBInfoMessageController.info-message}") private val infoMessage: String,
    @Autowired private val mockMvc: MockMvc,
    @Autowired private val wiremock: WireMockServer,
) {

    companion object {
        private const val READ_INFO_MESSAGE_B_AUTHORITY = "READ_INFO_MESSAGE_B"
        private const val INFO_MESSAGE_URL = "/info-message"
        private const val SERVICE_A_INFO_MESSAGE_URL = "/info-message"
    }

    @Test
    fun `given user is not authorized when info message endpoint is called then 401 is returned`() {
        mockMvc.get(INFO_MESSAGE_URL) { }
            .andDo { print() }
            .andExpect {
                status { isUnauthorized() }
                header { string("WWW-Authenticate", "Bearer") }
            }
    }

    @Test
    fun `given user is authorized but has no READ_INFO_MESSAGE_B authority when info message endpoint is called then 403 is returned`() {
        val authorization = jwt()

        mockMvc.get(INFO_MESSAGE_URL) { with(authorization) }
            .andDo { print() }
            .andExpect {
                status { isForbidden() }
                header {
                    string(
                        "WWW-Authenticate",
                        "Bearer error=\"insufficient_scope\", error_description=\"The request requires higher privileges than provided by the access token.\", error_uri=\"https://tools.ietf.org/html/rfc6750#section-3.1\""
                    )
                }
            }
    }

    @Test
    fun `given user is authorized and has READ_INFO_MESSAGE_B authority when info message endpoint is called then 200 and info message are returned`() {
        val authorization = jwt().authorities(SimpleGrantedAuthority(READ_INFO_MESSAGE_B_AUTHORITY))
        val serviceAInfoMessageResponse = "hello feign world"
        wiremock.get { url equalTo SERVICE_A_INFO_MESSAGE_URL } returns { body = serviceAInfoMessageResponse }

        mockMvc.get(INFO_MESSAGE_URL) { with(authorization) }
            .andDo { print() }
            .andExpect {
                status { isOk() }
                content {
                    contentTypeCompatibleWith(TEXT_PLAIN)
                    string(serviceAInfoMessageResponse + SPACE + infoMessage)
                }
            }
    }

    @Test
    fun `given user is authorized and has READ_INFO_MESSAGE_B authority when info message endpoint is called then service a info message endpoint is called`() {
        val authorization = jwt().authorities(SimpleGrantedAuthority(READ_INFO_MESSAGE_B_AUTHORITY))
        val serviceAInfoMessageResponse = "hello feign world"
        wiremock.get { url equalTo SERVICE_A_INFO_MESSAGE_URL } returns { body = serviceAInfoMessageResponse }

        mockMvc.get(INFO_MESSAGE_URL) { with(authorization) }
            .andDo { print() }
            .andExpect {
                status { isOk() }
                content {
                    contentTypeCompatibleWith(TEXT_PLAIN)
                    string(serviceAInfoMessageResponse + SPACE + infoMessage)
                }
            }

        wiremock.verify(
            getRequestedFor(urlEqualTo(SERVICE_A_INFO_MESSAGE_URL))
                .withHeader(AUTHORIZATION, equalTo("Bearer token"))
        )
    }
}
